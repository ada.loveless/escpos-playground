import numpy as np
import textwrap

from escpos import send
from escpos import qr
from escpos import patterns
from escpos import image
from pprint import pprint


def label(uri, name, id, date):
    img = qr.qr_code_image(uri)
    chunks = list(image.chunk_image(img))
    name = textwrap.wrap(name, 20)
    msg = b"\x1b@"
    msg += image.define_user_chars(chunks)
    msg += b"\x1b3\x18"
    msg += b"\x1b%\x01" + bytearray(range(32, 42)) + b"\x1b%\x00  "
    msg += name[0].encode() + b"\n"
    msg += b"\x1b%\x01" + bytearray(range(42, 52)) + b"\x1b%\x00  "
    if len(name) > 1:
        msg += name[1].encode()
    msg += b"\n"
    msg += b"\x1b%\x01" + bytearray(range(52, 62)) + b"\x1b%\x00  "
    if len(name) > 2:
        msg += name[2].encode()
    msg += b"\n"
    msg += b"\x1b%\x01" + bytearray(range(62, 72)) + b"\x1b%\x00  "
    msg += id.encode() + b"\n"
    msg += b"\x1b%\x01" + bytearray(range(72, 82)) + b"\x1b%\x00  "
    msg += date.encode() + b"\n"
    #msg += image.print_user_chars(range(32, 32+len(chunks)))
    msg += b"\n"
    #print(msg)
    return msg



def run():
    msg = label("https://example.org/a/000-M30W",
                "Ethernet Cable Cat6a 10m", "000-M30W", 
                "2023-09-12") 
    send.send_bytes("wall.local", msg)
