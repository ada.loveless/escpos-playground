from PIL import Image
import numpy as np


def get_image():
    image = Image.open("qr.png")
    return np.array(image)


def bits_to_byte(data):
    byte = 0
    for d in data:
        byte = (2 * byte) + d
    return byte


def chunk_image(img):
    chunk_height = 24
    chunk_width = 12
    xs = range(0, img.shape[0], chunk_height)
    ys = range(0, img.shape[1], chunk_width)
    for x in xs:
        for y in ys:
            chunk = img[x:x+chunk_height, y:y+chunk_width]
            yield chunk


def chunk_to_char(chunk):
    chunk = chunk.flatten("F").tolist()
    starts = range(0, len(chunk), 8)
    for start in starts:
        yield bits_to_byte(chunk[start:start+8])


def define_user_char(chunk):
    cmd = b"\x1b&"
    code = 32
    height, width = chunk.shape
    cmd += bytes([int(height/8), code, code, width])
    chars = list(chunk_to_char(chunk))
    for char in chars:
        cmd += char.to_bytes()
    return cmd


def define_user_chars(chunks):
    count = len(chunks)
    code_lo = 32
    code_hi = 32+count-1
    cmd = b"\x1b&"
    height, width = chunks[0].shape
    cmd += bytes([int(height/8), code_lo, code_hi])
    for chunk in chunks:
        cmd += width.to_bytes()
        for char in chunk_to_char(chunk):
            cmd += char.to_bytes()
    return cmd


def print_user_chars(codes):
    cmd = b"\x1b3\x18"
    cmd += b"\x1b%\x01"
    i = 1
    for code in codes:
        cmd += code.to_bytes()
        if (i % 10) == 0:
            cmd += b"\n"
        i += 1
    cmd += b"\x1b%%\x00"
    return cmd
