import random

BLOCK_CHAR = b"\xdb"
HEIGHT_SINGLE = b"\x1b3\x16"

def modes(content=b"\x1b@"):
    for i in range(255):
        content += b"\x1b!"
        content += i.to_bytes()
        content += f"Mode {i}\n".encode()
    return content

def block(width, height, content = b"\x1b@"):
    content = b""
    content += HEIGHT_SINGLE
    for line_n in range(height):
        for col_n in range(width):
            content += BLOCK_CHAR
        content += b"\n"
    content += b"\n"
    return content

def font_enlarge(max: int, content = b"\x1b@"):
    for i in range(128):
        content += b"\x1d!"
        content += i.to_bytes()
        content += f"{i}".encode()
        content += b"\n"
    return content

def test_page(content = b"\x1b@"):
    content += b"\x12T"
    return content

def noise(content = b"\x1b@", width=48, height=48):
    content += b"\x12*" + height.to_bytes() + width.to_bytes()
    for _ in range(width*height):
        content += random.randint(0, 255).to_bytes()
    content += b"\n\n\n\n"
    return content


def noise_wide(content = b"\x1b@", nl=48, nh=48):
    content += b"\x12V" + nl.to_bytes() + nh.to_bytes()
    for _ in range(nl+nh):
        content += random.randint(0, 255).to_bytes()
    content += b"\n"
    return content


POS = {
    "none": 0,
    "above": 1,
    "below": 2,
    "both": 3,
}

CODES = {
    "UPC-A": 0,
    "UPC-E": 1,
    "EAN13": 2,
    "EAN8": 3,
    "CODE39": 4,
    "I25": 5,
    "CODEBAR": 6,
    "CODE93": 7,
    "CODE128": 8,
    "CODE11": 9,
    "MSI": 10,
}

def barcode(x, height = 50, width= 3, pos = "below", code = "UPC-A", content = b"\x1b@"):
    # text position
    content += b"\x1dH" + POS[pos].to_bytes()
    # height
    content += b"\x1dh" + height.to_bytes()
    # width
    content += b"\x1dw" + width.to_bytes()
    # barcode content
    content += b"\x1dk" + CODES[code].to_bytes() 
    content += len(x).to_bytes()
    for c in x:
        content += int(c).to_bytes()
    content += b"\n"
    return content

