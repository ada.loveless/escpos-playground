import copy
import sys
import math
import random

import pyqrcode
import pprint
import numpy as np

def qr_code(content: str):
    qr = pyqrcode.create(content, error="L")
    return qr.code

def qr_code_image(content: str):
    code = np.array(qr_code(content))
    qr_size = code.shape[0]
    img = np.zeros((120, 120), 'bool')
    step = 120/qr_size
    starts = np.arange(0, 120, step)
    for (i, x) in enumerate(starts):
        for (j, y) in enumerate(starts):
            img[int(np.floor(x)):int(np.floor(x+step)),
                int(np.floor(y)):int(np.floor(y+step))] = code[i, j]
    return img
    

MAP_CP_437 = {
    (False, False): b" ",
    (False, True): b"\xdc",
    (True, False): b"\xdf",
    (True, True): b"\xdb",
}

def qr_to_cp437(content: list):
    """
    Takes two lines at a time and encodes them using 
    characters " ", "█", "▄" and "▀".
    """
    content = copy.deepcopy(content)
    out = b"\x1b3\x16" # Start with line-height at 24px (same as the character)
    while len(content) > 0:
        xs = content.pop()
        if len(content) > 0:
            ys = content.pop()
        else:
            ys = [0 for _ in range(len(xs))]
        for x, y in zip(xs, ys):
            out += MAP_CP_437[x, y]
        out += b"\n"
    out += b"\n\n\n\n"
    return out

def define_user_char(code=32, height=3, width=12):
    cmd = b"\x1b&%c%c%c%c" % (height, code, code, width)
    for i in range(height * width):
        cmd += (255).to_bytes() #random.randint(0, 255).to_bytes()
        #cmd += (i+32 % 255).to_bytes()
    return cmd

def print_user_char(code=32):
    cmd =  b"\x1b%%\xff%c\x1b%%\x00" % code 
    return cmd


def qr_to_chars(content: list):
    """
    Converts the QR code to custom characters.
    We can have at most 64 custom characters.
    As characters are 2x1 in shape, we can at most do 50 characters.
    This is a 10x5 grid of characters.
    As the characters on the printer are 12x24, this yields a 
    120x120 QR code.
    """
