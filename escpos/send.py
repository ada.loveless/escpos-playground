import subprocess

def send_bytes(target: str, content: bytes, cmd="/usr/bin/ssh", device="/dev/ttyS0"):
    proc = subprocess.Popen([cmd, target, "tee", device],
                            stdin=subprocess.PIPE, 
                            stdout=subprocess.PIPE)
    proc.communicate(input=content)
